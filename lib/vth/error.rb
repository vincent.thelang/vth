module Vth
  class Error < Vth::Base
    def self.rescue(e, request, params, session)
      error = new(e, request, params, session)
      error.push
      yield(error)
    end

    def self.raise(e, name=nil)
      error = new(e, nil, nil, nil, name)
      error.push
    end

    def initialize(e, request=nil, params=nil, session=nil, name=nil)
      @e = e
      @request = request
      @params = params
      @session = session
      @exception_code = 'ausstehend'
      @name = name
    end

    def push
      response = post("error_create", param)
      @exception_code = response["code"].to_s if response
    end

    def param
      @param = {
        error: {
          time: Time.zone.now,
          title: @e.inspect,
          backtrace: @e.backtrace&.join("\n"),
          is_404: is_404
        }
      }
      if @session.present?
        @param[:error][:session] = @session.to_json
      end
      if @params.present?
        @param[:error][:params] = filtered_params.to_json
      end
      if @request.present?
        @param[:error][:url] = @request.url
        @param[:error][:referer] = @request.env["HTTP_REFERER"]
        @param[:error][:agent] = @request.env["HTTP_USER_AGENT"]
        @param[:error][:ip] = @request.remote_ip
      else
        @param[:error][:url] = @name||"INTERN"
      end
      @param
    end

    def filtered_params
      if @params.present?
        filtered_params = @params.to_unsafe_h
        @params.to_unsafe_h.each do |key, value|
          if key.to_s.include?("password")
            filtered_params[key] = ("*" * value.size)
          end
        end
        filtered_params
      else
        {}
      end
    end

    def is_404
      arry = [
        "ActionController::UnknownController",
        "ActiveRecord::RecordNotFound",
        "Missing template",
        "ActionController::UnknownFormat",
        "ActionController::InvalidCrossOriginRequest",
        "ActionController::InvalidAuthenticityToken"
      ]
      arry.select{ |a| @e.inspect.include?(a) }.any?
    end

    def html
      path = Gem.loaded_specs['vth'].full_gem_path
      file = File.open("#{path}/vendor/500.html", "rb")
      content = file.read
      file.close
      content.gsub("%exception_code%", @exception_code)
    end
  end
end
