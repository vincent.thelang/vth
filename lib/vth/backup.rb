require 'dropbox_api'
class Vth::Backup < Vth::Base
  def initialize(date: nil, password: nil, dropbox_token: nil, directories: nil, root: nil, name: nil, back_up_count: nil, skip_database: false)
    @password = password || Rails.application.credentials.vth_backup[:password]
    @dropbox_token = dropbox_token || Rails.application.credentials.vth_backup[:dropbox_token]
    @directories = directories || ["storage"]
    @root = root || Rails.root.to_s
    @timestamp = (date || Time.now).to_date.to_s.gsub("-", "_")
    @name = name || Rails.application.class.module_parent_name
    @back_up_count = back_up_count || 5
    @skip_database = skip_database
  end

  def report
    @report ||= Vth::Base.new.post("backup_logs", {
      backup_log: {
        name: @name,
        start_at: Time.now
      }
    })
  end

  def run
    upload = create_zip do |zip_file|
      upload_to_dropbox(zip_file)
    end
    clear_dropbox
    Vth::Base.new.post("backup_logs/#{report["id"]}", {
      backup_log: {
        path: upload.path_display,
        size: upload.size / 1000000.0,
        end_at: Time.now,
        used: used
      }
    })
  end

  def create_zip
    Dir.mktmpdir do |dir|
      @dir = dir
      create_sql_dump unless @skip_database
      create_directories_dump
      `cd #{@dir} && zip -9 --password '#{@password}' #{@name}_#{@timestamp}.zip . -r`
      yield("#{@dir}/#{@name}_#{@timestamp}.zip")
    end
  end

  def upload_to_dropbox(file)
    File.open(file) do |f|
      client.upload_by_chunks "/#{@name}/#{@name}_#{@timestamp}.zip", f
    end
  end

  def clear_dropbox
    result = client.list_folder "/#{@name}"
    if result.entries.length > @back_up_count
      result.entries.sort{|x| x.client_modified}.last(result.entries.length-@back_up_count).each do |entry|
        client.delete(entry.path_display)
      end
    end
  end

  def create_directories_dump
    @directories.each do |dir|
      if dir.start_with?("/")
        FileUtils.cp_r dir, @dir
      else
        FileUtils.cp_r "#{root}/#{dir}", @dir
      end
    end
  end

  def create_sql_dump
    db = Rails.configuration.database_configuration[Rails.env]["database"]
    `cd #{@dir} && pg_dump -U rails #{db} > #{db}_#{@timestamp}`
  end

  def root
    if @root.include?("/releases/")
      @root.split("/releases/").first + "/shared"
    else
      @root
    end
  end

  def used
    x = client.get_space_usage
    x.used.to_f / x.allocation.allocated * 100
  end

  def client
    return @client if @client.present?

    access_token = if report["hash_data"].present?
      authenticator = DropboxApi::Authenticator.new(Rails.application.credentials.vth_backup[:client_id], Rails.application.credentials.vth_backup[:client_secret])
      OAuth2::AccessToken.from_hash(authenticator, JSON.parse(crypt.decrypt_and_verify(report["hash_data"])) )
    else
      authenticator = DropboxApi::Authenticator.new(Rails.application.credentials.vth_backup[:client_id], Rails.application.credentials.vth_backup[:client_secret])
      puts authenticator.auth_code.authorize_url(token_access_type: 'offline')
      a = gets.chomp
      authenticator.auth_code.get_token(a)
    end
    
    Vth::Base.new.post("backup_logs/#{report["id"]}", {
      backup_log: {
        hash_data: crypt.encrypt_and_sign(access_token.to_hash.to_json)
      }
    })

    @client = DropboxApi::Client.new(
      access_token: access_token,
    )
  end


  def crypt
    return @crypt if @crypt.present?

    len   = ActiveSupport::MessageEncryptor.key_len
    key   = ActiveSupport::KeyGenerator.new(Rails.application.credentials.secret_key_base).generate_key(Rails.application.credentials.vth_backup[:password], len)
    @crypt = ActiveSupport::MessageEncryptor.new(key)
  end
end
