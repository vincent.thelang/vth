module Vth
  class Message < Vth::Base
    def initialize(name:, email:, subject:, body: nil, fields: [], agent: nil, ip: nil, referer: nil)
      @name = name
      @email = email
      @subject = subject
      @body = body || to_body(fields)
      @agent = agent
      @ip = ip
      @referer = referer
    end

    def save
      post("message_create", params)
    end

    private

    def to_body(fields)
      return if fields.blank?
      fields.map do |name, value|
        "<strong>#{name}</strong><br>#{value}"
      end.join("<br><br>")
    end

    def params
      {
        message: {
          name: @name,
          email: @email,
          subject: @subject,
          body: @body,
          agent: @agent,
          ip: @ip,
          referer: @referer
        }
      }
    end
  end
end
