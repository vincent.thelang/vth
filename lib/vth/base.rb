require 'httparty'
module Vth
  class Base
    def post(path, attributes = {})
      attributes[:token] = Rails.application.config.note_key
      attributes[attributes.keys.first][:project_id] = Rails.application.config.project_id
      response = HTTParty.post(url(path), body: attributes)
      if response.created?
        JSON.parse(response.body)
      else
        @errors = JSON.parse(response.body)["errors"]
        false
      end
    end

    def errors
      @errors
    end

    private

    def url(path)
      "#{Rails.application.config.note_host}/api/#{path}"
    end
  end
end
