$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "vth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "vth"
  s.version     = Vth::VERSION
  s.authors     = ["Vincent Thelang"]
  s.email       = ["vincent@vincent-thelang.de"]
  s.homepage    = "https://vincent-thelang.de"
  s.summary     = "Summary of Vth."
  s.description = "Description of Vth."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails"
  s.add_dependency "httparty"
  s.add_dependency "dropbox_api"
end
